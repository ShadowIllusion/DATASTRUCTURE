#ifndef 顺序栈
#define 顺序栈

const int MaxSize = 50;

template <typename Elemtype>
struct SqStack
{
    Elemtype data[MaxSize]; // 存放栈中元素
    int top;                // 栈顶指针
};

template <typename T>
void InitStack(SqStack<T> &s)
{
    s.top = -1;
}

template <typename T>
bool StackEmpty(const SqStack<T> &S)
{
    return S.top == -1;
}

template <typename T>
bool Push(SqStack<T> &S, T x)
{
    if (S.top == MaxSize - 1)
    {
        return false;
    }
    else
    {
        S.data[++S.top] = x;
        return true;
    }
}

template <typename T>
bool Pop(SqStack<T> &S, T &x)
{
    if (StackEmpty(S))
    {
        return false;
    }
    else
    {
        x = S.data[S.top--];
    }
    return true;
}

template <typename T>
bool GetTop(SqStack<T> &S, T &x)
{
    if (StackEmpty(S))
    {
        return false;
    }
    else
    {
        x = S.data[S.top];
        return true;
    }
}

#endif